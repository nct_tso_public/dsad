#Script for reading images and multi-label annotations from NRRD format, merges annotations via STAPLE and saves the results as png.
#2021 Sebastian Bodenstedt Email: firstname.lastname@nct-dresden.de
import SimpleITK as sitk
import sys
import os
import numpy as np
import cv2
import json
import numpy as np
from pycocotools import mask

cl_to_id = {'abdominal wall' : 0,
            'spleen' : 1,
            'inferior esenteric vein' : 2, 
            'pancreas' : 3, 
            'colon': 4, 
            'inferior emsenteric vein' : 2,
            'inferior mesnteric vein' : 2,
            'small intestine' : 5,
            'liver' : 6,
            'stomach' : 7,
            'inferior mesenteric vein': 2}

id_to_cl = {0 : 'abdominal wall',
            1 : 'spleen',
            2 : 'inferior mesenteric vein', 
            3 : 'pancreas', 
            4 : 'colon', 
            5 : 'small intestine',
            6 : 'liver',
            7 : 'stomach'}

target_width = 1920
target_height = 1080

annotations = []
annotations_np = []
for id in id_to_cl:
    annotations.append([])
    annotations_np.append([])

op_name = os.path.splitext(os.path.basename(sys.argv[-2]))[0]
out_folder = sys.argv[-1] + "/" + op_name
os.makedirs(out_folder, exist_ok=True)
os.makedirs(out_folder + "/merged", exist_ok=True)

for i in range(len(sys.argv) - 3):
    os.makedirs(out_folder + "/anno_%d" % (i+1), exist_ok=True)
    target_obj = sitk.ReadImage(sys.argv[i+1])

    width = target_obj.GetWidth()
    height = target_obj.GetHeight()
    depth = target_obj.GetDepth()

    np_image = sitk.GetArrayFromImage(target_obj)
    if len(np_image.shape) == 3:
        np_image = np.expand_dims(np_image,3)
        dims = 1
    else:
        dims = np_image.shape[3]

    if width != target_width or height != target_height:
        image = np.zeros((depth,target_height, target_width,dims), dtype=np.uint8)

        target_origin = target_obj.GetOrigin()[:-1]
        start_x = int(target_origin[0])
        start_y = int(target_origin[1])
        end_x = target_width - width - start_x
        end_y = target_height - height - start_y

        if end_y == 0:
            end_y = -target_height
        if end_x == 0:
            end_x = -target_width

        image[:,start_y:-end_y, start_x:-end_x, :] = np_image

        np_image = image
    
    complex = "Segment0_Layer" in target_obj.GetMetaDataKeys()

    count = 0
    name = "Segment%d_Name" % count
    done = not name in target_obj.GetMetaDataKeys()
    classes = {}
    while not done:
        if complex:
            layer = int(target_obj.GetMetaData("Segment%d_Layer" % count))
            label = int(target_obj.GetMetaData("Segment%d_LabelValue" % count))
            id = (layer, label)
        else:
            id = (count, 1)
        cl = cl_to_id[target_obj.GetMetaData(name)]

        classes[cl] = id

        count += 1
        name = "Segment%d_Name" % count
        done = not name in target_obj.GetMetaDataKeys()

        
    for cl in classes:
        layer, label = classes[cl]
        indices = (np_image[:,:,:,layer] == label)


        anno = np.zeros((depth, target_height, target_width), dtype=np.uint8)
        print(cl, label, np.sum(indices))
        anno[indices] = 1
        annotations_np[cl].append(anno)
        annotations[cl].append(sitk.GetImageFromArray(anno))

print("Start staple")
merged = []
staple_filter = sitk.STAPLEImageFilter()

for id in id_to_cl:
    if len(annotations[id]) > 0:
        staple_image = staple_filter.Execute(annotations[id])
        target = sitk.GetArrayFromImage(staple_image)
    else:
        target = None

    merged.append(target)

images = sitk.ReadImage(sys.argv[-2])
images = sitk.GetArrayFromImage(images)

count = 0

for i in range(images.shape[0]):
    for id in range(len(merged)):
        if merged[id] is None:
            continue
        slice = merged[id][i]
        slice_binary = (slice > 0.9).astype(dtype=np.uint8)*255

        cv2.imwrite(out_folder + "/merged/mask%02d_%s.png" % (i, id_to_cl[id]), slice_binary)
          
        for j in range(len(annotations_np[id])):
            anno = annotations_np[id][j][i].astype(dtype=np.uint8)*255
            cv2.imwrite(out_folder + "/anno_%d/mask%02d_%s.png" % (j+1,i, id_to_cl[id]), anno)
            
    cv2.imwrite(out_folder + "/image%02d.png" % i, cv2.cvtColor(images[i], cv2.COLOR_RGB2BGR))
