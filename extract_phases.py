#Extracting script for DSAD dataset. Extracts frames from a surgical video based on a b<>com [annotate] annotation
#2021 Sebastian Bodenstedt Email: firstname.lastname@nct-dresden.de

import sys
import os
import numpy as np
import subprocess
import cv2
import xml.etree.ElementTree as ET

path = os.path.dirname(os.path.realpath(__file__))

print(path)
path_ffmpeg = os.path.join(path, "ffmpeg.exe") #assumes that ffmpeg.exe (under windows) is in the same folder as the script.
#path_ffmpeg = "ffmpeg" #use under linux if ffmpeg is installed globally
types = {}

#Pancreas
items = []
items.append(("effector", ["visibility 1", "visibility 2"]))
items.append(("target", ["pancreas"]))

types["pancreas"] = items

#vesicular gland
items = []
items.append(("effector", ["visibility 1", "visibility 2"]))
items.append(("target", ["vesicular gland"]))

types["vesicular gland"] = items

#Intestinal Veins
items = []
items.append(("effector", ["visibility 1", "visibility 2"]))
items.append(("target", ["Inferior Mesenteric Vein"]))

types["Intestinal Veins"] = items

#Inferior Mesenteric Artery
items = []
items.append(("effector", ["visibility 1", "visibility 2"]))
items.append(("target", ["Inferior Mesenteric Artery"]))

types["Inferior Mesenteric Artery"] = items

#Ureter
items = []
items.append(("effector", ["visibility 1", "visibility 2"]))
items.append(("target", ["ureter"]))

types["ureter"] = items

#Stomach
items = []
items.append(("effector", ["visibility 1", "visibility 2"]))
items.append(("target", ["stomach"]))

types["stomach"] = items

#Spleen
items = []
items.append(("effector", ["visibility 1", "visibility 2"]))
items.append(("target", ["spleen"]))

types["spleen"] = items

#Liver
items = []
items.append(("effector", ["visibility 1", "visibility 2"]))
items.append(("target", ["liver"]))

types["liver"] = items

def extract_objs(file_name):
    f = open(file_name, "r")
    lines = f.readlines() 

    splits = [[]]
    for line in lines:
        if line[0:3] == "===":
            splits.append([])

        splits[-1].append(line)
    
    splits = splits[1:]

    objs = []

    for split in splits:
        obj = {}
        obj["type"] = split[0][4:-5]

        for line in split:
            pos = line.find(":")
            if pos >= 0:
                a_name = line[:pos].replace(" ", "")
                a_value = line[pos+2:-1]
                if "\"" in a_value:
                    a_value = a_value.replace("\"", "")
                elif a_value.isnumeric():
                    a_value = int(a_value)
                obj[a_name] = a_value
        objs.append(obj)
    return objs


def MakeVideo(video_file, snippets, outfile, fps):
    complex_filer = ""
    if len(snippets) > 1:
        complex_filer += "[0:v]scale=1920:1080[anno];[anno] split=%d" % len(snippets)
        for i in range(len(snippets)):
            complex_filer += "[anno%d]" % i
        complex_filer += ";"
        concat = ""
        for i in range(len(snippets)):
            complex_filer += "[anno%d]trim=start=%d:end=%d,setpts=PTS-STARTPTS[v%d];" % (i,
                snippets[i][0], snippets[i][1], i)
            concat += "[v%d]" % i

        complex_filer = complex_filer + concat + "concat=n=%d:[out1]" % len(snippets)
    else:
        complex_filer = "[0:v]scale=1920:1080[anno];[anno]trim=start=%d:end=%d,setpts=PTS-STARTPTS[out1]" % (
                snippets[0][0], snippets[0][1])

    command = [path_ffmpeg, "-i", video_file, "-r" , str(fps), "-filter_complex", complex_filer, "-map",
                "[out1]", os.path.join(outfile, "image_%04d.png")]
    print(command)
    subprocess.run(command)

csv_file = sys.argv[1]

f = open(csv_file)
lines = f.readlines()
f.close()

splits = lines[0].split(",")
video_file = splits[0]
anno_file = splits[1]
output_folder = splits[2][:-1]

objs = extract_objs(anno_file)
for l in range(1, len(lines)):
    splits = lines[l].split(",")
    print(splits)
    if len(splits) < 2:
        break

    cl = splits[0]
    fps = float(splits[1])
    print(cl, fps)
    snippets = []

    for obj in objs:
        if obj["type"] == "Physical Action":
            use = True
            for item in types[cl]:
                if not (item[0] in obj and obj[item[0]] in item[1]):
                    use = False
                    break
            if use:
                snippets.append((obj["startTime"]/1000, obj["stopTime"]/1000))
                print(snippets[-1])
    if len(snippets) == 0:
        continue

    folder = os.path.join(output_folder, cl)
    print(folder)
    os.makedirs(folder, exist_ok=True)
    MakeVideo(video_file, snippets,folder, fps)
