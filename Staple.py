#Script for reading images and binary annotations from NRRD format, merges annotations via STAPLE and saves the results as png.
#2021 Sebastian Bodenstedt Email: firstname.lastname@nct-dresden.de
import SimpleITK as sitk
import sys
import os
import numpy as np
import cv2

targets_list = []
targets_np_list = []

target_width = 1920
target_height = 1080

op_name = os.path.splitext(os.path.basename(sys.argv[-2]))[0]
out_folder = sys.argv[-1] + "/" + op_name
os.makedirs(out_folder, exist_ok=True)
os.makedirs(out_folder + "/merged", exist_ok=True)

for i in range(len(sys.argv) - 3):
    os.makedirs(out_folder + "/anno_%d" % (i+1), exist_ok=True)
    target_obj = sitk.ReadImage(sys.argv[i+1])

    width = target_obj.GetWidth()
    height = target_obj.GetHeight()
    depth = target_obj.GetDepth()

    if width != target_width or height != target_height:
        image = np.zeros((depth,target_height, target_width), dtype=np.uint8)
        np_image = sitk.GetArrayFromImage(target_obj)
        if len(np_image.shape) == 4:
            np_image = np_image[:,:,:,0]

        target_origin = target_obj.GetOrigin()[:-1]
        start_x = int(target_origin[0])
        start_y = int(target_origin[1])
        end_x = target_width - width - start_x
        end_y = target_height - height - start_y

        image[:,start_y:-end_y, start_x:-end_x] = np_image

        targets_list.append(sitk.GetImageFromArray(image))
    else:
        targets_list.append(target_obj)

    targets_np_list.append(sitk.GetArrayFromImage(targets_list[-1]))

staple_filter = sitk.STAPLEImageFilter()

staple_image = staple_filter.Execute(targets_list)

target = sitk.GetArrayFromImage(staple_image)


images = sitk.ReadImage(sys.argv[-2])
images = sitk.GetArrayFromImage(images)

for i, slice in enumerate(target):
    slice_binary = (slice > 0.9).astype(dtype=np.uint8)*255
    cv2.imwrite(out_folder + "/merged/mask%02d.png" % i, slice_binary)

    for j in range(len(targets_np_list)):
        anno = targets_np_list[j][i].astype(dtype=np.uint8)*255
        cv2.imwrite(out_folder + "/anno_%d/mask%02d.png" % (j+1,i), anno)

    cv2.imwrite(out_folder + "/image%02d.png" % i, cv2.cvtColor(images[i], cv2.COLOR_RGB2BGR))

