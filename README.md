# Dresden Surgical Anatomy Dataset

This git contains the most relevant scripts used to create the [Dresden Surgical Anatomy Dataset](https://doi.org/10.6084/m9.figshare.21702600).

## Extraction from video

Data was extracted from videos collected from robot-assisted colorectal surgeries using the `extract_phases.py` script. As input it expects a configuration file (such as `example.csv`), which should contain the path to the video and to an annotation using [b\<\>com \[annotate\]](https://b-com.com) where the visibility of organs is annotated. Furthermore it needs to list with organs to extract with the frame rate (FPS) to extract.

## Merge annotations
Assuming the resulting images of the prior step were annotated using [itk snap](http://www.itksnap.org), the script `Staple.py` (`Staple_multi.py` for multi-label segmentation) can be used to extract the data from the resulting NRRD files and create merged segmentations using STAPLE.

## Multi-rater evaluation
The `MultiRater.py` script (`MultiRater_multi.py` for multi-label segmentation) can be used to compare the annotations of the different annotators to the final annotation by computing the DICE coefficient and the normalized Hausdorff distance.

## Credit
If you use or re-use parts of the code, please refer to our DOI: 10.5281/zenodo.6958337
