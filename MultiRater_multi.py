#Multi-label Mutltirater agreement for DSAD dataset. Computes the agreement of the 3 raters and merged dataset with the final annotation of the multi-label subset.
#2021 Sebastian Bodenstedt Email: firstname.lastname@nct-dresden.de
import sys
import os
import numpy as np
import cv2
import numpy as np
import glob
from skimage.metrics import hausdorff_distance
import math
from cmath import isinf

data_dir = "/mnt/ceph/tco/TCO-Students/Projects/cobot_seg_final/multilabel/"

def hd(im1, im2): #compute normalized hausdorff distance
    d = hausdorff_distance(im1, im2)
    if isinf(d):
        d = 1
    else:
        d = d/math.sqrt(im1.shape[0]*im1.shape[0] + im1.shape[1]*im1.shape[1])
    return d

def dice(im1, im2): #compute dice score
    im1 = np.asarray(im1).astype(np.bool)
    im2 = np.asarray(im2).astype(np.bool)

    if im1.shape != im2.shape:
        raise ValueError("Shape mismatch: im1 and im2 must have the same shape.")

    if im1.sum() + im2.sum() == 0:
        return -1

    intersection = np.logical_and(im1, im2)

    return 2. * intersection.sum() / (im1.sum() + im2.sum())

sub_dirs_c = os.listdir(data_dir)
sub_dirs = []
for i in range(len(sub_dirs_c)):
    if os.path.isdir(os.path.join(data_dir,sub_dirs_c[i])):
        sub_dirs.append(sub_dirs_c[i])

results_log = open("multilabel.csv", "w")

for dir_id in range(len(sub_dirs)):
    files = glob.glob(data_dir + sub_dirs[dir_id] + "/mask*.png")
    files.sort()

    for i in range(len(files)):
        if "stomach" in files[i]:
            continue
        final_mask = cv2.imread(files[i], cv2.IMREAD_GRAYSCALE)
        results_log.write("multilabel_" + sub_dirs[dir_id] + (",%02d," % i))

        for j in range(4):
            if j == 0:
                anno_id = "merged/"
            else:
                anno_id = "anno_%d/" % j
            
            file = files[i].replace("mask", anno_id + "mask")
            mask = cv2.imread(file, cv2.IMREAD_GRAYSCALE)

            d = dice(final_mask, mask)
            h_d = hd(final_mask, mask)
            if d < 0:
                results_log.write("n/a,n/a,")
                continue
            results_log.write("%.2f,%.2f," % (d, h_d))
            results_log.flush()
        results_log.write("\n")
results_log.close()